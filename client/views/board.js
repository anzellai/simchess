// Empty position dictionary for board creation
// after chess movement, we capture on board pieces and insert into this
// so the dictionary keys: values pattern will be always the same
EMPTY_POSITION = {
    "09": "", A9: "", B9: "", C9: "", D9: "", E9: "", F9: "", G9: "", H9: "", I9: "",
    "08": "", A8: "", B8: "", C8: "", D8: "", E8: "", F8: "", G8: "", H8: "", I8: "",
    "07": "", A7: "", B7: "", C7: "", D7: "", E7: "", F7: "", G7: "", H7: "", I7: "",
    "06": "", A6: "", B6: "", C6: "", D6: "", E6: "", F6: "", G6: "", H6: "", I6: "",
    "05": "", A5: "", B5: "", C5: "", D5: "", E5: "", F5: "", G5: "", H5: "", I5: "",
    "04": "", A4: "", B4: "", C4: "", D4: "", E4: "", F4: "", G4: "", H4: "", I4: "",
    "03": "", A3: "", B3: "", C3: "", D3: "", E3: "", F3: "", G3: "", H3: "", I3: "",
    "02": "", A2: "", B2: "", C2: "", D2: "", E2: "", F2: "", G2: "", H2: "", I2: "",
    "01": "", A1: "", B1: "", C1: "", D1: "", E1: "", F1: "", G1: "", H1: "", I1: "",
    "00": "", A0: "", B0: "", C0: "", D0: "", E0: "", F0: "", G0: "", H0: "", I0: ""
};
// EMPTY is to be used to fill the array value of "FROM" after moving FROM to TO
EMPTY = '';
// BOARD cooridinates declaration
BOARD_COLS = {false:['0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'], true:['I','H', 'G', 'F', 'E', 'D', 'C', 'B', 'A', '0']};
BOARD_ROWS = {false:['9', '8', '7', '6', '5', '4', '3', '2', '1', '0'], true:['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']};

// Using already converted unicode chess pieces speeds up rendering
// PIECE_SET is a dictionary to convert fen => position array
PIECE_SET = {
    pawn:'pawn', prince:'prince', castle:'castle', knight:'knight', bishop:'bishop', king:'king', queen:'queen', princess:'princess', mind:'mind', environment:'environment', genetics:'genetics', quarks1:'quarks1', quarks2:'quarks2', sun:'sun', moon:'moon',
    PAWN:'PAWN', PRINCE:'PRINCE', CASTLE:'CASTLE', KNIGHT:'KNIGHT', BISHOP:'BISHOP', KING:'KING', QUEEN:'QUEEN', PRINCESS:'PRINCESS', MIND:'MIND', ENVIRONMENT:'ENVIRONMENT', GENETICS:'GENETICS', QUARKS1:'QUARKS1', QUARKS2:'QUARKS2', SUN:'SUN', MOON:'MOON',
};

Session.setDefault("reverse_board", false);

Template.board.helpers({
    'cols': function() {
        return BOARD_COLS[Session.get("reverse_board")];
    },
    'rows': function() {
	    return BOARD_ROWS[Session.get("reverse_board")];
    },
    'board': function() {
        var boardHtml = '<table id="chess_board" cellpadding="0" cellspacing="0">';
        var reverse = Session.get("reverse_board");
        var game = Games.findOne({});
        if (!game)
            return;
        var pieces = game.POSITION;
        for (var row in BOARD_ROWS[reverse]) {
            var rowHtml = "<tr>";
            for (var col in BOARD_COLS[reverse]) {
                var pos = BOARD_COLS[reverse][col].toUpperCase() + BOARD_ROWS[reverse][row].toUpperCase();
                var pc = pieces[pos.toUpperCase()] || pieces[pos.toLowerCase()];
                //var lastplay = (pos == lastmove[0] || pos == lastmove[1]) ? "lastplayed" : "";
                var pieceHtml = pc ? '<div class=\"piece ' + pc + '\" draggable=true title=\"' + pc + '\" piece=\"' + pc + '\"</div>' : "";
                rowHtml = rowHtml + '<td class=\"dropzone\" id=' + pos + '>' + pieceHtml + '</td>';
            }
            boardHtml = boardHtml + rowHtml + "</tr>";
        }
        boardHtml += "</table>";
        return boardHtml;
    },
    'history': function() {
        var game = Games.findOne({});
        if (!game)
            return;
        var history = game.HISTORY;
        if (history.length !== 0) {
            Meteor.defer(function() {
                var sourceId = history[history.length-1].sourceId;
                var targetId = history[history.length-1].targetId;
                console.log(sourceId, targetId);
                $('.clicked').remove();
                $('#'+sourceId).addClass("clicked");
                $('#'+targetId).addClass("clicked");
            });
        }
        return history;
    },
    'message': function() {
        return Messages.find({}, {sort: {"timestamp": -1}}).fetch();
    }
});

Template.board.events = {
    'dragstart [draggable=true]': function(e) {
        e.originalEvent.dataTransfer.setData("source_id", e.target.parentNode.id);
        e.originalEvent.dataTransfer.setData("piece", $(e.target).attr("piece"));
    },
    'dragover .dropzone': function(e) {
        e.preventDefault();
    },
    'drop .dropzone': function(e) {
        var targetId = e.target.id ? e.target.id : e.target.parentNode.id;
        var sourceId = e.originalEvent.dataTransfer.getData("source_id");
        var piece = e.originalEvent.dataTransfer.getData("piece");
        if (sourceId == targetId) {
            e.originalEvent.dataTransfer.setData("source_id", null);
            e.originalEvent.dataTransfer.setData("piece", null);
        }
        if (sourceId !== targetId && sourceId !== null && piece !== null) {
            Meteor.call('movePiece', piece, sourceId, targetId);
        }
        return;
    },
    /**
     * Click & Drop implementation using Jquery
     * TODO: sourceId is not stored even setting on Session variable "clicked"

    'click .piece': function(e) {
        try {
            var targetId = e.target.id ? e.target.id : e.target.parentNode.id;
        } catch (e) {
            Session.set("piece_clicked", null);
            Session.set("clicked", null);
            $('.clicked').remove();
            return;
        }

        if (Session.get("piece_clicked") == null) {
            Session.set("piece_clicked", $(e.target).attr("piece"));
            Session.set("clicked", targetId);
            $(e.target).addClass("clicked");
            return;
        }

        if (Session.get("piece_clicked") == $(e.target).attr("piece") || Session.get("clicked") == targetId) {
            Session.set("piece_clicked", null);
            Session.set("clicked", null);
            $(".clicked").remove();
            return;
        }

        Meteor.call('movePiece', Session.get("piece_clicked"), Session.get("clicked"), targetId, function(err,res) {
            if (err)
                throw err;
            Session.set("piece_clicked", null);
            Session.set("clicked", null);
        });
    },
    'click .dropzone': function(e) {
        try {
            var targetId = e.target.id ? e.target.id : e.target.parentNode.id;
        } catch (e) {
            Session.set("piece_clicked", null);
            Session.set("clicked", null);
            $('.clicked').remove();
            return;
        }
        if (Session.get("piece_clicked") == null || Session.get("clicked") == targetId) {
            Session.set("clicked", null);
            $('.clicked').remove();
            return;
        }
        Meteor.call('movePiece', Session.get("piece_clicked"), Session.get("clicked"), targetId, function(err,res) {
            if (err)
                throw err;
            Session.set("piece_clicked", null);
            Session.set("clicked", null);
        });
    },
    */

    'click .rotate': function() {
    	Session.set("reverse_board", !Session.get("reverse_board"));
        Meteor.defer(function() {
            Template.board.history();
        });
        return;
    },
    'click .reset': function() {
    	Meteor.call('reset');
    	return;
    },
    'click .chat': function() {
        var content = $('#message-input').val();
        if (content !== "") {
            return Meteor.call('chat', content, function(err, res) {
                if (err)
                    throw err;
                return $('#message-input').val("");
            });
        }
        return;
    },
    'click div[class^="hint"]': function() {
        $('div[class^="hint-"]').fadeOut(50);
    },
    'click .PAWN, click .pawn': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-pawn').fadeIn(50);
    },
    'click .PRINCE, click .prince, click .PRINCESS, click .princess': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-prince-and-princess').fadeIn(50);
    },
    'click .CASTLE, click .castle': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-castle').fadeIn(50);
    },
    'click .KNIGHT, click .knight': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-knight').fadeIn(50);
    },
    'click .BISHOP, click .bishop': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-bishop').fadeIn(50);
    },
    'click .QUEEN, click .queen': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-queen').fadeIn(50);
    },
    'click .KING, click .king': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-king').fadeIn(50);
    },
    'click .MIND, click .mind': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-mind').fadeIn(50);
    },
    'click .ENVIRONMENT, click .environment': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-environment').fadeIn(50);
    },
    'click .GENETICS, click .genetics': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-genetics').fadeIn(50);
    },
    'click .QUARKS1, click .quarks1, click .QUARKS2, click .quarks2': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-quarks').fadeIn(50);
    },
    'click .MOON, click .moon': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-moon').fadeIn(50);
    },
    'click .SUN, click .sun': function() {
        $('div[class^="hint-"]').hide();
        $('.hint-sun').fadeIn(50);
    },


}
