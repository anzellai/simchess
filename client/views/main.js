Session.setDefault('board_ready', false);
Session.setDefault('reverse_board', false);
Session.setDefault('clicked', null);
Session.setDefault('piece_clicked', null);
Session.setDefault('show_tutorial', false);

// globally bind tap + touchend with click event
$(window).on('tap, touchend', function(e) {
    e.preventDefault();
    return $(e.target).click();
});

Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL'
});

Tracker.autorun(function() {
    if (Meteor.user()) {
        Meteor.subscribe('games');
        Meteor.subscribe('messages');
    }
})

Template.splash.helpers({
    show_tutorial: function(){
        return Session.get('show_tutorial');
    }
})

Template.splash.events = {
    'click .btn-show-tutorial': function() {
        var show_tutorial = !Session.get('show_tutorial');
        Session.set('show_tutorial', show_tutorial);
        var tutorial_handle = Meteor.setInterval(function(){
            if (typeof Template.hint !== undefined) {
                $('.hint').css({'position': 'relative'}).fadeIn();
                $('div[class^="hint-"]').fadeIn();
                Meteor.clearInterval(tutorial_handle);
            }
        }, 100);

    }
}
