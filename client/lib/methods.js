movePiece = function(piece, sourceId, targetId) {
	console.log(piece, sourceId, targetId);
	if (piece == null) return;
	var game = Games.findOne({});
	if (!game) return;
	var position = game.POSITION;
	position[targetId] = piece;
	position[sourceId] = "";
	var history = game.HISTORY;
	history.push({"piece": piece, "sourceId": sourceId, "targetId": targetId});
	var message = game.MESSAGE;
	Games.update(game._id, {"POSITION": position, "HISTORY": history, "MESSAGE": message});
	return;
}