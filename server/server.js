Meteor.publish('games', function() {
	return Games.find();
});

Meteor.publish('messages', function() {
	return Messages.find();
});

Meteor.startup(function() {
	if (Games.find().count() == 0) {
		Games.insert({
			POSITION: {
				"09": "mind", A9: "environment", B9: "genetics", C9: "quarks2", D9: "sun", E9: "moon", F9: "quarks1", G9: "genetics", H9: "environment", I9: "mind",
				"08": "prince", A8: "castle", B8: "knight", C8: "bishop", D8: "king", E8: "queen", F8: "bishop", G8: "knight", H8: "castle", I8: "princess",
				"07": "pawn", A7: "pawn", B7: "pawn", C7: "pawn", D7: "pawn", E7: "pawn", F7: "pawn", G7: "pawn", H7: "pawn", I7: "pawn",
				"06": "", A6: "", B6: "", C6: "", D6: "", E6: "", F6: "", G6: "", H6: "", I6: "",
				"05": "", A5: "", B5: "", C5: "", D5: "", E5: "", F5: "", G5: "", H5: "", I5: "",
				"04": "", A4: "", B4: "", C4: "", D4: "", E4: "", F4: "", G4: "", H4: "", I4: "",
				"03": "", A3: "", B3: "", C3: "", D3: "", E3: "", F3: "", G3: "", H3: "", I3: "",
				"02": "PAWN", A2: "PAWN", B2: "PAWN", C2: "PAWN", D2: "PAWN", E2: "PAWN", F2: "PAWN", G2: "PAWN", H2: "PAWN", I2: "PAWN",
				"01": "PRINCE", A1: "CASTLE", B1: "KNIGHT", C1: "BISHOP", D1: "KING", E1: "QUEEN", F1: "BISHOP", G1: "KNIGHT", H1: "CASTLE", I1: "PRINCESS",
				"00": "MIND", A0: "ENVIRONMENT", B0: "GENETICS", C0: "QUARKS1", D0: "MOON", E0: "SUN", F0: "QUARKS2", G0: "GENETICS", H0: "ENVIRONMENT", I0: "MIND"
			},
			HISTORY: [],
		});
	}
	if (Messages.find().count() == 0) {
		Messages.insert({
			CONTENT: "Welcome to SimChess!",
			timestamp: new Date()
		});
	}
});

Meteor.methods({
	'reset': function() {
		Games.update(Games.findOne({})._id, {
			POSITION: {
				"09": "mind", A9: "environment", B9: "genetics", C9: "quarks2", D9: "sun", E9: "moon", F9: "quarks1", G9: "genetics", H9: "environment", I9: "mind",
				"08": "prince", A8: "castle", B8: "knight", C8: "bishop", D8: "king", E8: "queen", F8: "bishop", G8: "knight", H8: "castle", I8: "princess",
				"07": "pawn", A7: "pawn", B7: "pawn", C7: "pawn", D7: "pawn", E7: "pawn", F7: "pawn", G7: "pawn", H7: "pawn", I7: "pawn",
				"06": "", A6: "", B6: "", C6: "", D6: "", E6: "", F6: "", G6: "", H6: "", I6: "",
				"05": "", A5: "", B5: "", C5: "", D5: "", E5: "", F5: "", G5: "", H5: "", I5: "",
				"04": "", A4: "", B4: "", C4: "", D4: "", E4: "", F4: "", G4: "", H4: "", I4: "",
				"03": "", A3: "", B3: "", C3: "", D3: "", E3: "", F3: "", G3: "", H3: "", I3: "",
				"02": "PAWN", A2: "PAWN", B2: "PAWN", C2: "PAWN", D2: "PAWN", E2: "PAWN", F2: "PAWN", G2: "PAWN", H2: "PAWN", I2: "PAWN",
				"01": "PRINCE", A1: "CASTLE", B1: "KNIGHT", C1: "BISHOP", D1: "KING", E1: "QUEEN", F1: "BISHOP", G1: "KNIGHT", H1: "CASTLE", I1: "PRINCESS",
				"00": "MIND", A0: "ENVIRONMENT", B0: "GENETICS", C0: "QUARKS1", D0: "MOON", E0: "SUN", F0: "QUARKS2", G0: "GENETICS", H0: "ENVIRONMENT", I0: "MIND"
			},
			HISTORY: []
		});
		Messages.remove({}, function(err,res) {
			if (err)
				throw err;
			Messages.insert({
				CONTENT: "Welcome to SimChess!",
				timestamp: new Date()
			});
		});
		return;
	},
	'chat': function(content) {
		if (content !== "") {
			Messages.insert({"CONTENT": content, "timestamp": new Date()});
		}
		return;
	},
	movePiece: function(piece, sourceId, targetId) {
		if (piece == null) return;
		var game = Games.findOne({});
		if (!game) return;
		var position = game.POSITION;
		position[targetId] = piece;
		position[sourceId] = "";
		var history = game.HISTORY;
		history.push({"piece": piece, "sourceId": sourceId, "targetId": targetId});
		Games.update(game._id, {$set: {"POSITION": position, "HISTORY": history}});
		return;
	}
});